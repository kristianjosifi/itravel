﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ITravel
{
    public partial class UdhetimiIm : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string drejtues = Session["Username"].ToString();

            int usermarres = Convert.ToInt32(Server.HtmlEncode(Request.QueryString["id"]));

            string connectionString = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection lidhje = new SqlConnection(connectionString))
            {
                string query = "SELECT drejtuesi,nrpass,nisja,mberritja,vendnisja,destinacioni,foto,rate FROM udhetim WHERE id=@id";
                SqlCommand cmd = new SqlCommand(query, lidhje);
                cmd.Parameters.AddWithValue("@id", usermarres);
                lidhje.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                DataList1.DataSource = rdr;
                DataList1.DataBind();
            }
            using (SqlConnection lidhje1 = new SqlConnection(connectionString))
            {
                string query1 = "SELECT marka,tipi,viti,nrvendeve,photo FROM mjeti WHERE drejtuesid=(select id from person where username=@emr)";
                SqlCommand cmd1 = new SqlCommand(query1, lidhje1);
                cmd1.Parameters.AddWithValue("@emr", drejtues);
                lidhje1.Open();
                SqlDataReader rdr1 = cmd1.ExecuteReader();
                DataList2.DataSource = rdr1;
                DataList2.DataBind();
            }
        }

        protected void Fshi(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Server.HtmlEncode(Request.QueryString["id"]));
            string connectionString = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection lidhje = new SqlConnection(connectionString))
            {
                string query = "delete from udhetim where id=@id ";
                SqlCommand cmd = new SqlCommand(query, lidhje);
                cmd.Parameters.AddWithValue("@id", id);
                lidhje.Open();
                cmd.ExecuteNonQuery();
                Response.Redirect("UdhetimetEMia.aspx");

            }
        }

        protected void Update(object sender, EventArgs e)
        {
            int usermarres = Convert.ToInt32(Server.HtmlEncode(Request.QueryString["id"]));
            string useri = Session["Username"].ToString();
            string roli = Session["Roli"].ToString();
            string s = @"~\foto\" + IMAZHI.FileName;
            IMAZHI.PostedFile.SaveAs(Server.MapPath(s));
            string connectionString = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string shto = "update udhetim set nrpass=@nrpass ,nisja=@nisja ,mberritja=@mberritja ,vendnisja=@vendnisja, destinacioni=@destinacioni, foto=@photo where id=@id";
                SqlCommand cmd = new SqlCommand(shto, con);
                cmd.Parameters.AddWithValue("@nrpass", nrpass.Text);
                cmd.Parameters.AddWithValue("@vendnisja", vendnisja.Text);
                cmd.Parameters.AddWithValue("@nisja", nisja.Text);
                cmd.Parameters.AddWithValue("@roli", roli);
                cmd.Parameters.AddWithValue("@useri", useri);
                cmd.Parameters.AddWithValue("@mberritja", mberritja.Text);
                cmd.Parameters.AddWithValue("@destinacioni", dest.Text);
                cmd.Parameters.AddWithValue("@id", usermarres);
                cmd.Parameters.AddWithValue("@photo", s);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                Response.Redirect("UdhetimetEMia.aspx");
            }
        }

    }
}