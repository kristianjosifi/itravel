﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="ITravel.Admin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>

#menu li a:hover span span, #menu #menu_active5 a span span {
	background:url(../images/menu_bg.gif) top repeat-x
}
    </style>
    <div style="padding-left:30px; padding-top:10px; background-color:#0094B0; ">

    <asp:GridView ID="GridView1" CssClass="Grid" Width="625px" runat="server" AutoGenerateColumns="False"  
        AllowPaging="true" OnRowCancelingEdit="GridView1_RowCancelingEdit1" 
        onpageindexchanging="GridView1_PageIndexChanging"   
        onrowediting="GridView1_RowEditing" onrowupdating="GridView1_RowUpdating"  DataKeyNames="id" > 
      <Columns> 
        <asp:CommandField ShowEditButton="True" /> 
        <asp:BoundField DataField="id" HeaderText="ID e Perdoruesit" ReadOnly="True"  
            SortExpression="PersonID" /> 
        <asp:TemplateField HeaderText="Username" SortExpression="Username"> 
            <ItemTemplate> 
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Username") %>'></asp:Label> 
            </ItemTemplate> 
        </asp:TemplateField> 
        <asp:TemplateField HeaderText="Roli" SortExpression="Roli"> 
            <EditItemTemplate> 
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Roli") %>'></asp:TextBox> 
            </EditItemTemplate> 
            <ItemTemplate> 
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Roli") %>'></asp:Label> 
            </ItemTemplate> 
        </asp:TemplateField> 
       </Columns> 
    </asp:GridView> 
     
    <asp:LinkButton ID="hiqPerdorues" ForeColor="black" runat="server" onclick="shfaq_Panel">Hiq Perdorues</asp:LinkButton>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp      
    <asp:Label ID="Label6" ForeColor="black" runat="server" Text=""></asp:Label>             <br />
 
     <asp:Panel ID="pnlHiq" runat="server" Visible="False"> <br />
       <asp:Label ID="Label2" runat="server" ForeColor="black" Text="Username"></asp:Label>
         <asp:TextBox ID="Username1" runat="server"></asp:TextBox>  <br /> <br /> 
        <asp:Button ID="hiq" runat="server" onclick="heqje" Text="Hiq"></asp:Button>   
         <asp:Button ID="cancel" runat="server" onclick="cancel_Click" Text="Cancel"></asp:Button>    
     </asp:Panel> 
     <asp:Label ID="Lbl" runat="server" Text=""></asp:Label>    <br />    
    
    <asp:Label ID="Label4" runat="server" ForeColor="black" Text=""></asp:Label><br />  

    <asp:GridView ID="GridView2" CssClass="Grid" runat="server" Width="630px" AutoGenerateColumns="False"  
        AllowPaging="true" OnRowCancelingEdit="GridView1_RowCancelingEdit1" 
          onpageindexchanging="GridView1_PageIndexChanging"   
    onrowediting="GridView1_RowEditing"   DataKeyNames="id" > 
    <Columns> 
        <asp:CommandField ShowEditButton="True" /> 
        <asp:BoundField DataField="id" HeaderText="ID" ReadOnly="True"  
            SortExpression="PersonID" /> 
                <asp:TemplateField HeaderText="drejtuesi" SortExpression="KATEGORIA"> 
            <ItemTemplate> 
                <asp:Label ID="Label2" runat="server" Text='<%# Bind("drejtuesi") %>'></asp:Label> 
            </ItemTemplate> 
              </asp:TemplateField> 
                <asp:TemplateField HeaderText="Tipi" SortExpression="MARKA"> 
            <ItemTemplate> 
                <asp:Label ID="Label3" runat="server" Text='<%# Bind("roli") %>'></asp:Label> 
            </ItemTemplate> 
              </asp:TemplateField> 
               
       </Columns> 
    </asp:GridView> 
     
    <asp:LinkButton ID="hiqProdukt" ForeColor="Red" runat="server" onclick="shfaq_Panel"> 
    <asp:Label ID="Label7" runat="server" ForeColor="black" Text="Hiq Udhetim"></asp:Label></asp:LinkButton>  &nbsp&nbsp &nbsp&nbsp &nbsp&nbsp &nbsp&nbsp &nbsp&nbsp
        <asp:Label ID="Label1" ForeColor="black" runat="server" Text=""></asp:Label>      <br />

     <asp:Panel ID="Panel1" runat="server" Visible="False"> <br />
       <asp:Label ID="Label8" runat="server" ForeColor="black" Text="Id"></asp:Label>
         <asp:TextBox ID="id" runat="server"></asp:TextBox>     <asp:Label ID="Label9" runat="server" ForeColor="black" Text=""></asp:Label>    <br /> <br /> 
        <asp:Button ID="Button1" runat="server" onclick="heqje1" Text="Hiq"></asp:Button>   
         <asp:Button ID="Button2" runat="server" onclick="cancel_Click" Text="Cancel"></asp:Button>    
     </asp:Panel> <br /> 
</div>
</asp:Content>
