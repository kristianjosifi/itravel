﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Configuration;
using System.Web.Security;

namespace ITravel
{
    public partial class Admin : System.Web.UI.Page
    {
        private static String connSTR = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["roli"] as string))
            {

            }
            else
            {
                if (Session["roli"].ToString() == "user")
                {
                    Response.Redirect("Kryesore.aspx");
                }

                if (!IsPostBack)
                {
                    AfishoGridView();
                    AfishoGridView1();
                }
            }
        }

        private void AfishoGridView()
        {
            using (SqlConnection conn = new SqlConnection(connSTR))
            {
                string query = "SELECT * FROM person where roli!='admin'";
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connSTR))
            {
                string query = "UPDATE person SET Roli = @roli WHERE id = @ID and roli != 'admin'";
                SqlCommand cmd = new SqlCommand(query, conn);
                string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
                string roli = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("TextBox1")).Text;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = Convert.ToInt32(id);
                cmd.Parameters.AddWithValue("@roli", roli);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            GridView1.EditIndex = -1;
            AfishoGridView();
            hiqPerdorues.Visible = true;
        }

        protected void heqje(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Username1.Text))
            {
                using (SqlConnection conn = new SqlConnection(connSTR))
                {
                    string query = "delete from person where username=@username AND roli!='admin'";
                    string query1 = "delete from udhetim where useri=@username";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    SqlCommand cmd1 = new SqlCommand(query1, conn);
                    cmd.Parameters.AddWithValue("@username", Username1.Text);
                    cmd1.Parameters.AddWithValue("@username", Username1.Text);
                    conn.Open();
                    cmd1.ExecuteNonQuery();
                    int rowsAffected = cmd.ExecuteNonQuery();
                    if (rowsAffected == 0)
                    {
                        Label6.Text = "Ky udhetim nuk ekziston";
                    }
                    else
                    {
                        Label6.Text = "Udhetimi u fshi me sukses";
                    }
                }
                AfishoGridView();
                pastro();
            }
            else { Lbl.Text = "Username eshte i detyrueshem"; }
        }
      

        private void AfishoGridView1()
        {
            using (SqlConnection conn = new SqlConnection(connSTR))
            {
                string query = "SELECT* FROM udhetim where username!=@useri and roli != 'admin'";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@useri", Session["Username"].ToString());
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                GridView2.DataSource = ds;
                GridView2.DataBind();
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            AfishoGridView();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            AfishoGridView();
            hiqProdukt.Visible = false;
        }

        protected void heqje1(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(id.Text))
            {
                using (SqlConnection conn = new SqlConnection(connSTR))
                {
                    string query = "delete from udhetim where id=@id and username=@user";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@id", id.Text);
                    cmd.Parameters.AddWithValue("@user", Session["Username"].ToString());
                    conn.Open();
                    int rowsAffected = cmd.ExecuteNonQuery();
                    if (rowsAffected == 0)
                    {
                        Label1.Text = "Ju nuk mund ta fshini kete udhetim";
                    }
                    else
                    {
                        Label1.Text = "Udhetimi u fshi me sukses";
                    }
                }
                AfishoGridView();
                pastro();
            }
            else { Lbl.Text = "Id eshte e detyrueshme"; }
        }

        private void pastro()
        {
            id.Text = "";
            hiqProdukt.Visible = true;
            pnlHiq.Visible = false;
            Lbl.Text = "";
        }

        protected void cancel_Click(object sender, EventArgs e)
        {
            pastro();
        }

        protected void shfaq_Panel(object sender, EventArgs e)
        {
            hiqProdukt.Visible = false;
            pnlHiq.Visible = true;
        }

        protected void GridView1_RowCancelingEdit1(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            AfishoGridView();
            hiqProdukt.Visible = true;
        }
    }
}