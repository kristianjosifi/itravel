﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Web.Security;

namespace ITravel
{
    public partial class UdhetimetEMia : Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Afisho();
        }

        private void Afisho()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection lidhje = new SqlConnection(connectionString))
            {

                string usr = Session["Username"].ToString();
                string query = "SELECT id,drejtuesi,nrpass,nisja,mberritja,vendnisja,destinacioni,foto FROM udhetim WHERE username=@usr ";
                SqlCommand cmd = new SqlCommand(query, lidhje);
                cmd.Parameters.AddWithValue("@usr", usr);
                lidhje.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                DataList1.DataSource = rdr;
                DataList1.DataBind();
       
            }
        }

        protected void kontaktouser(object sender, EventArgs e)
        {
            LinkButton Label8 = (LinkButton)sender;
            Response.Redirect("UdhetimiIm.aspx?id=" + Label8.Text);
        }
    }
}