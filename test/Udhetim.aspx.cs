﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ITravel
{
    public partial class Udhetim : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string drejtues = Session["Username"].ToString();

            int usermarres = Convert.ToInt32(Server.HtmlEncode(Request.QueryString["id"]));

            string connectionString = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection lidhje = new SqlConnection(connectionString))
            {
                string query = "SELECT drejtuesi,nrpass,nisja,mberritja,vendnisja,destinacioni,foto,rate FROM udhetim WHERE id=@id";
                SqlCommand cmd = new SqlCommand(query, lidhje);
                cmd.Parameters.AddWithValue("@id", usermarres);
                lidhje.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                DataList1.DataSource = rdr;
                DataList1.DataBind();
            }
            using (SqlConnection lidhje1 = new SqlConnection(connectionString))
            {
                string query1 = "SELECT marka,tipi,viti,nrvendeve,photo FROM mjeti WHERE drejtuesid=(select id from person where username=(select username from udhetim where id=@id1))";
                SqlCommand cmd1 = new SqlCommand(query1, lidhje1);
                cmd1.Parameters.AddWithValue("@id1", usermarres);
                lidhje1.Open();
                SqlDataReader rdr1 = cmd1.ExecuteReader();
                DataList2.DataSource = rdr1;
                DataList2.DataBind();
            }
        }
        protected void btnRating_Click(object sender, EventArgs e)
        {
            int rat = Convert.ToInt32(rblRating.SelectedItem.Value);
            string usermarres = Server.HtmlEncode(Request.QueryString["id"]).ToString();
            string connectionString = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string shtorate = "update udhetim set rate=(rate+@rate)/2 where id=@id";
                SqlCommand cmd3 = new SqlCommand(shtorate, con);
                cmd3.Parameters.AddWithValue("@rate", rat);
                cmd3.Parameters.AddWithValue("@id", usermarres);
                con.Open();
                cmd3.ExecuteNonQuery();

            }
        }
        protected void reservo(object sender, EventArgs e)
        {
            string drejtues = Session["Username"].ToString();
            string usermarres = Server.HtmlEncode(Request.QueryString["id"]).ToString();
            string connectionString = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {

                string upd = "if not exists (select * from udhetim_person where idudhetimi=@id1 and emerudhetari=@emr) begin if not exists (select * from udhetim where id=@id1 and nrpass=0) begin update udhetim set nrpass=nrpass-1 where id=@id1 end end";
                SqlCommand cmd1 = new SqlCommand(upd, con);
                cmd1.Parameters.AddWithValue("@id1", usermarres);
                cmd1.Parameters.AddWithValue("@emr", drejtues);
                con.Open();
                cmd1.ExecuteNonQuery();

                string shto = "if not exists (select * from udhetim_person where idudhetimi=@id and emerudhetari=@emr) begin if not exists (select * from udhetim where id=@id and nrpass=0) begin INSERT INTO udhetim_person(idudhetimi,emerudhetari) VALUES(@id,@emr) end end";
                SqlCommand cmd = new SqlCommand(shto, con);
                cmd.Parameters.AddWithValue("@id", usermarres);
                cmd.Parameters.AddWithValue("@emr", drejtues);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        protected void Fshi()
        {
            int id = Convert.ToInt32(Server.HtmlEncode(Request.QueryString["id"]));
            string connectionString = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection lidhje = new SqlConnection(connectionString))
            {
                string query = "delete from udhetime where id=@id ";
                SqlCommand cmd = new SqlCommand(query, lidhje);
                cmd.Parameters.AddWithValue("@id", id);
                lidhje.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                DataList1.DataSource = rdr;
                DataList1.DataBind();

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string usermarres = Server.HtmlEncode(Request.QueryString["id"]).ToString();
            string password=passw.Text;
            string usrdergues = Session["Username"].ToString();
            string ConStr = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(ConStr))
            {
                string emailmarres = "select email from person where username=(select username from udhetim where id=@id)";
                string emaildergues = "select email from person where username=@dergues";
                SqlCommand gjejemail = new SqlCommand(emaildergues, cn);
                SqlCommand gjejemailmarres = new SqlCommand(emailmarres, cn);
                gjejemail.Parameters.AddWithValue("@dergues", usrdergues);
                gjejemailmarres.Parameters.AddWithValue("@id", usermarres);
                cn.Open();
                string emaili = gjejemail.ExecuteScalar().ToString();
                string emailimarres = gjejemailmarres.ExecuteScalar().ToString();
                dergoEmail(emailimarres, emaili, password);
            }
        }

        private void dergoEmail(string marresi, string derguesi, string password)
        {
            MailMessage email = new MailMessage(derguesi, marresi);
            StringBuilder trupiEmail = new StringBuilder();
            trupiEmail.Append(TextArea1.Text);
            email.IsBodyHtml = true;
            email.Body = trupiEmail.ToString();
            email.Subject = "Kerkese per Udhetim";
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.Credentials = new System.Net.NetworkCredential()
            {
                UserName = derguesi,
                Password = password
            };
            client.EnableSsl = true;
            try
            {
                client.Send(email);
                Lbl.Text = "Email u dergua me sukses";
            }
            catch
            {
                Lbl.Text = "Passwordi juaj nuk perputhet me emailin";
            }
        }
    }
}   