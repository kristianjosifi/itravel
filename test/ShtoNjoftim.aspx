﻿<%@ Page Title="Shto Njoftim" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShtoNjoftim.aspx.cs" Inherits="ITravel.ShtoNjoftim" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<style>
#menu li a:hover span span, #menu #menu_active3 a span span {
	background:url(../images/menu_bg.gif) top repeat-x
}
</style>
     <div style="padding-left:30px; padding-top:10px; background-color:#0094B0;">
      <div style="background-color:#0094B0 ; z-index:9; position:relative; top:10px; "> <asp:Label ID="Label3" runat="server" ForeColor="white" Font-Size="50px" Text="Shto Udhetim" ></asp:Label>  </br> </div> </br>     </br> 
       <table cellspacing=4 cellpadding=2 width=100% >
       <tr>
            <td class="style1"><asp:Label ID="Label4" ForeColor="black" runat="server" Text="Jepni Numrin e Pasagjereve" ></asp:Label></td>
            <td>
               <asp:DropDownList id="nrpass" runat="server" BorderColor="gray" BorderWidth="1px">
                  <asp:ListItem Selected="True" Value="1"> 1 </asp:ListItem>
                  <asp:ListItem Value="2"> 2 </asp:ListItem>
                  <asp:ListItem Value="3"> 3 </asp:ListItem>
                  <asp:ListItem Value="4"> 4 </asp:ListItem>
                  <asp:ListItem Value="5">5 </asp:ListItem>
                  <asp:ListItem Value="6">6 </asp:ListItem>
                  <asp:ListItem Value="7">7 </asp:ListItem>
                   <asp:ListItem Value="8">8 </asp:ListItem>
                   <asp:ListItem Value="9">9 </asp:ListItem>
                   <asp:ListItem Value="10">10 </asp:ListItem>
                   <asp:ListItem Value="11">11 </asp:ListItem>
                   <asp:ListItem Value="12">12 </asp:ListItem>
               </asp:DropDownList>    </td>
        </tr>
        <tr>
            <td class="style1"><asp:Label ID="Label1" ForeColor="black" runat="server" Text="Jepni Nisjen" ></asp:Label>  </td>
            <td>
                <asp:TextBox ID="nisja" runat="server" TextMode="Date" BorderColor="gray" BorderWidth="1px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="nisja" 
                    ErrorMessage="Jepni Nisjen" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1"><asp:Label ID="Label2" ForeColor="black" runat="server" Text="Jepni Mberritjen" ></asp:Label></td>
            <td>
                <asp:TextBox ID="mberritja" runat="server" TextMode="Date" BorderColor="gray" BorderWidth="1px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="mberritja" 
                    ErrorMessage="Jepni Mberritjen" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1"><asp:Label ID="Label8" ForeColor="black" runat="server" Text="Jepni Vendnisjen" ></asp:Label></td>
            <td>
                <asp:TextBox ID="vendnisja" runat="server" BorderColor="gray" BorderWidth="1px" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="vendnisja" 
                    ErrorMessage="Jepni Vendnisjen" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1"><asp:Label ID="Label9" ForeColor="black" runat="server" Text="Jepni Destinacionin" ></asp:Label></td>
            <td>
                <asp:TextBox ID="dest" runat="server" BorderColor="gray" BorderWidth="1px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="dest" 
                    ErrorMessage="Jepni Destinacionin" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
       
        <tr>
            <td class="style1"><asp:Label ID="Label5" ForeColor="black" runat="server" Text="Jepni Foton" ></asp:Label></td>
            <td>
                <asp:FileUpload ID="IMAZHI"  runat="server" ForeColor="black" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="red" runat="server" 
                    ControlToValidate="IMAZHI" ErrorMessage="Jepni nje imazh"></asp:RequiredFieldValidator>
            </td>
        </tr>
</table> <br />
    <asp:Button ID="Ruaj" runat="server"  Text="Ruaj" onclick="ruaj_Click"/>
    <asp:Label ID="Label6" runat="server" Text="" ForeColor="black"></asp:Label><br /><br />
 </div>
</asp:Content>
