﻿<%@ Page Title="Udhetimet E Mia" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UdhetimetEMia.aspx.cs" Inherits="ITravel.UdhetimetEMia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        #menu li a:hover span span, #menu #menu_active2 a span span {
	background:url(../images/menu_bg.gif) top repeat-x
}
        .auto-style1 {
             position:relative;
             text-align:center;
             padding:10px;
             width:100%;
            
        }
        .auto-style2 {
            position:relative;
            text-align:center;
            left:8px;
            top:50px;
            background-color:#0094B0;
            border:3px solid white;
            border-radius:10px 10px;
            /*padding:10px;*/
             width:100%;
             float: left;
}
        .tedhena{
            float:left;
            width: 100%;
            height:170px;
        }
        .fotot{
             width:350px;
             height:150px;
             padding:10px;
             float: left;
        }
        #divv{
            position:relative;
              width:400px;
             height:150px;
             float: left;
        }
        .emri{
            color:black;
            font-size:15px;
            /*padding:10px 0 10px 0;*/
            text-align:center;
            font-weight:bold;
            float: left;
        }
        .shiko{
            height:70px;
            width:150px;
            position:relative;
            top:-100px;
            left:150px;
            border:5px solid green;
        }
        .log{color:firebrick;}
        .men{width:100px;}
        body{
             background-repeat:repeat-x;
             
         }
    </style>
             
        <div class="wrapper pad1" style="background-color:#0094B0" >
               <div style="background-color:#0094B0; z-index:9; position:relative; top:-10px; left:20px"> <asp:Label ID="Label9" runat="server" ForeColor="white" Font-Size="50px" Text="Udhetimet E Mia" ></asp:Label>  </br>   </div> 
        <asp:DataList ID="DataList1" style="top:-30px; position:relative" runat="server" GridLines="Horizontal" RepeatColumns="1" Width="100%" RepeatDirection="Horizontal" BackColor="#0094B0"  CellPadding="4"  >
              <ItemTemplate>
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style2">
                            <div class="tedhena">
                                 <div id="divv"><asp:Image ID="foto" runat="server" CssClass="fotot" ImageUrl='<%# Eval("FOTO") %>'/></div><br />
                       
                            <asp:Label ID="Label8" runat="server" style="display:none" Text='<%# Eval("id") %>'></asp:Label>   
                            <asp:Label ID="Label1" runat="server" CssClass="emri" Text="Drejtuesi: &nbsp;&nbsp;&nbsp;"></asp:Label><asp:Label ID="drejtuesi" runat="server" CssClass="emri" Text='<%# Eval("drejtuesi") %>'></asp:Label>     <br />
                            <asp:Label ID="Label2" runat="server" CssClass="emri" Text="Numer Pasagjeresh: &nbsp;&nbsp;&nbsp;"></asp:Label> <asp:Label ID="nrpass" runat="server" CssClass="emri" Text='<%# Eval("nrpass") %>'></asp:Label><br />
                            <asp:Label ID="Label3" runat="server" CssClass="emri" Text="Data Nisjes: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="nisja" runat="server" CssClass="emri" Text='<%# Eval("nisja") %>'></asp:Label><br />
                            <asp:Label ID="Label5" runat="server" CssClass="emri" Text="Data Mberritjes: &nbsp;&nbsp;&nbsp;"></asp:Label> <asp:Label ID="mberritja" runat="server" CssClass="emri" Text='<%# Eval("mberritja") %>'></asp:Label><br />
                            <asp:Label ID="Label6" runat="server" CssClass="emri" Text="Vendi Nisjes: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="vendnisja" runat="server" CssClass="emri" Text='<%# Eval("vendnisja") %>'></asp:Label><br />
                            <asp:Label ID="Label7" runat="server" CssClass="emri" Text="Destinacioni: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="destinacioni" runat="server" CssClass="emri" Text='<%# Eval("destinacioni") %>'></asp:Label>   <br />
                                      
                            <asp:ImageButton ID="ImageButton1" runat="server"  CssClass="shiko" OnClick="kontaktouser" ImageUrl='images/shiko.jpg' PostBackUrl='<%#"UdhetimiIm.aspx?id="+Eval("id") %>'/>
                             
                                </div>
                          
                            </td>
                   
                      </tr>

                    <tr>
                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle  />
        </asp:DataList>
    </div>

</asp:Content>
