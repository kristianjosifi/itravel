﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Web.Security;

namespace ITravel
{
    public partial class Kryesore : Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Afisho();
            }
        }

        protected void search_TextChanged(object sender, EventArgs e)
        {
            Kerko();
        }

        private void Kerko()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection lidhje = new SqlConnection(connectionString))
            {
                string usr = Session["Username"].ToString();
                string query = "SELECT id,drejtuesi,nrpass,nisja,mberritja,vendnisja,destinacioni,foto FROM udhetim WHERE username!=@usr and (roli=@rol or nisja=@nisja or mberritja=@mberritja or nrpass=@nrpass or vendnisja=@vendnisja or destinacioni=@destinacioni) ";
                SqlCommand cmd = new SqlCommand(query, lidhje);
                cmd.Parameters.AddWithValue("@rol", rol.SelectedItem.Value.ToString());
                cmd.Parameters.AddWithValue("@nrpass", nrpass.Text.ToString());
                cmd.Parameters.AddWithValue("@usr", usr);
                cmd.Parameters.AddWithValue("@vendnisja", vendnisja.Text.ToString());
                cmd.Parameters.AddWithValue("@nisja", nisja.Text.ToString());
                cmd.Parameters.AddWithValue("@mberritja", mberritja.Text.ToString());
                cmd.Parameters.AddWithValue("@destinacioni", destinacioni.Text.ToString());
                lidhje.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                DataList1.DataSource = rdr;
                DataList1.DataBind();
       
            }
        }

        private void Afisho()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection lidhje = new SqlConnection(connectionString))
            {
                string usr = Session["Username"].ToString();
                string query = "SELECT id,drejtuesi,nrpass,nisja,mberritja,vendnisja,destinacioni,foto FROM udhetim WHERE username!=@usr ";
                SqlCommand cmd = new SqlCommand(query, lidhje);
                cmd.Parameters.AddWithValue("@usr", usr);
                lidhje.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                DataList1.DataSource = rdr;
                DataList1.DataBind();

            }
        }

        protected void kontaktouser(object sender, EventArgs e)
        {
            LinkButton Label8 = (LinkButton)sender;
            Response.Redirect("Udhetim.aspx?id=" + Label8.Text); 
        }
    }
}