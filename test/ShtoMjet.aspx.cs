﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Configuration;

namespace ITravel
{
    public partial class ShtoMjet : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ruaj_Click(object sender, EventArgs e)
        {
            string drejtues = Session["Username"].ToString();
            string s = @"~\foto\" + IMAZHI.FileName;
            IMAZHI.PostedFile.SaveAs(Server.MapPath(s));
            string connectionString = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string shto = "INSERT INTO mjeti(nrshasie,marka ,tipi ,viti ,nrvendeve ,photo,drejtuesid )VALUES(@nrshasie, @marka, @tipi, @viti, @nrvendeve, @photo, (select id from person where username=@usr))";
                SqlCommand cmd = new SqlCommand(shto, con);
                cmd.Parameters.AddWithValue("@nrshasie", shasi.Text);
                cmd.Parameters.AddWithValue("@marka", marka.Text);
                cmd.Parameters.AddWithValue("@tipi", tipi.Text);
                cmd.Parameters.AddWithValue("@nrvendeve", nrpass.Text);
                cmd.Parameters.AddWithValue("@viti", viti.Text);
                cmd.Parameters.AddWithValue("@usr", drejtues);
                cmd.Parameters.AddWithValue("@photo", s);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                Label6.Text = "Mjeti u Shtua";
            }
        }
    }
}