﻿<%@ Page Title="Udhetim" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Udhetim.aspx.cs" Inherits="ITravel.Udhetim" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <style>

.map1{
    width: 420px;
    height: 475px;
    top: 10px;
    float: left;
}
.auto-style1 {
             position:relative;
           
             padding:10px;
             width:100%;
        }
.auto-style2 {
            position:relative;
           
            left:8px;
            top:7px;

             width:100%;
             float: left;
}
        .tedhena{
            float:left;
            width: 100%;
        }
        .fotot{
             width:450px;
             height:300px;
             padding:10px;
             float: left;
        }

        #mbaj{
            position:relative;
                   float:left;
        }
        .emri{
            color:black;
            font-size:15px;
            /*padding:10px 0 10px 0;*/
            text-align:center;
            font-weight:bold;
            float: left;
        }
        .bli{
            height:30px;
            width:30px;
        }
        .res {
            height:90px;
            width:200px;
            border:5px solid blue;
            position:relative;
        }
        .cmimi{
            bottom:20px;
            color:white;
            font-size:20px;
            padding:0px;
            text-align:center;
            font-weight:bold;
        }
        .log{color:firebrick;}
        .men{width:100px;}
        body{background-repeat:repeat-x;}
        #trup{ background-color:#0094B0;}
</style>
       <div id="trup">
        <asp:DataList ID="DataList1" runat="server" GridLines="Horizontal" RepeatColumns="1" Width="100%" RepeatDirection="Horizontal"  CellPadding="4"  >
              <ItemTemplate>
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style2">
                                </br>
                            <asp:Label ID="Label8" runat="server" ForeColor="black" Text="Detajet e Udhetimit" Font-Size="20px"></asp:Label></br>   </br>
                            <div id="divv"><asp:Image ID="foto" runat="server" CssClass="fotot" ImageUrl='<%# Eval("FOTO") %>'/></div><br />
                            <div id="mbaj">
                            <asp:Label ID="Label10" runat="server" CssClass="emri" Font-Size="30px" ForeColor="red" Text="Rate: &nbsp;&nbsp;&nbsp;"></asp:Label><asp:Label ID="Label11" ForeColor="red" Font-Size="30px" runat="server" CssClass="emri" Text='<%# Eval("rate") %>'></asp:Label><asp:Label ID="Label12" runat="server" CssClass="emri" Font-Size="30px" ForeColor="red" Text="&nbsp;/&nbsp;5"></asp:Label>     <br /> <br /><br />
                            <asp:Label ID="Label1" runat="server" CssClass="emri" Text="Drejtuesi: &nbsp;&nbsp;&nbsp;"></asp:Label><asp:Label ID="drejtuesi" runat="server" CssClass="emri" Text='<%# Eval("drejtuesi") %>'></asp:Label>     <br />
                            <asp:Label ID="Label2" runat="server" CssClass="emri" Text="Numer Pasagjeresh: &nbsp;&nbsp;&nbsp;"></asp:Label> <asp:Label ID="nrpass" runat="server" CssClass="emri" Text='<%# Eval("nrpass") %>'></asp:Label><br />
                            <asp:Label ID="Label3" runat="server" CssClass="emri" Text="Data Nisjes: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="nisja" runat="server" CssClass="emri" Text='<%# Eval("nisja") %>'></asp:Label><br />
                            <asp:Label ID="Label5" runat="server" CssClass="emri" Text="Data Mberritjes: &nbsp;&nbsp;&nbsp;"></asp:Label> <asp:Label ID="mberritja" runat="server" CssClass="emri" Text='<%# Eval("mberritja") %>'></asp:Label><br />
                            <asp:Label ID="Label6" runat="server" CssClass="emri" Text="Vendi Nisjes: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="vendnisja" runat="server" CssClass="emri" Text='<%# Eval("vendnisja") %>'></asp:Label><br />
                            <asp:Label ID="Label7" runat="server" CssClass="emri" Text="Destinacioni: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="destinacioni" runat="server" CssClass="emri" Text='<%# Eval("destinacioni") %>'></asp:Label>   <br />       <br />
                             </div>  </br>
          
                            </td>
                      </tr>
                    <tr>
                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle  />
        </asp:DataList>

        <asp:DataList ID="DataList2" runat="server" GridLines="Horizontal" RepeatColumns="1" Width="100%" RepeatDirection="Horizontal"  CellPadding="4"  >
              <ItemTemplate>
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style2">
                        
                            <asp:Label ID="Label8" runat="server" ForeColor="black" Text="Detajet e Automjetit" Font-Size="20px"></asp:Label></br>   </br>
                            <div id="divv"><asp:Image ID="foto" runat="server" CssClass="fotot" ImageUrl='<%# Eval("photo") %>'/></div><br />
                            <div id="mbaj">
                            <asp:Label ID="Label1" runat="server" CssClass="emri" Text="Marka: &nbsp;&nbsp;&nbsp;"></asp:Label><asp:Label ID="drejtuesi" runat="server" CssClass="emri" Text='<%# Eval("marka") %>'></asp:Label>     <br />
                            <asp:Label ID="Label2" runat="server" CssClass="emri" Text="Tipi: &nbsp;&nbsp;&nbsp;"></asp:Label> <asp:Label ID="nrpass" runat="server" CssClass="emri" Text='<%# Eval("tipi") %>'></asp:Label><br />
                            <asp:Label ID="Label3" runat="server" CssClass="emri" Text="Viti: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="nisja" runat="server" CssClass="emri" Text='<%# Eval("viti") %>'></asp:Label><br />
                            <asp:Label ID="Label5" runat="server" CssClass="emri" Text="Numri Vendeve: &nbsp;&nbsp;&nbsp;"></asp:Label> <asp:Label ID="mberritja" runat="server" CssClass="emri" Text='<%# Eval("nrvendeve") %>'></asp:Label><br />
                            </div>  </br>
               
                            </td>
                      </tr>
                    <tr>
                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle  />
        </asp:DataList>

      <div style="padding-left:30px; padding-top:10px; border-radius:15px 15px; width:500px; position:relative " >
            <asp:Label ID="Label8" runat="server" ForeColor="black" Text="Kontakto" Font-Size="20px"></asp:Label></br>   </br></br>
            <asp:Label ID="Label1" runat="server" ForeColor="black" Text="Fusni passwordin e emailit tuaj"></asp:Label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label3" runat="server" ForeColor="black" Text="Ju po kontaktoni:"></asp:Label>  
            <asp:Label ID="kontakt" runat="server" ForeColor="black" Text=""></asp:Label><br /><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
            Text="*" ErrorMessage="Fusni Passwordin" ControlToValidate="passw"
            ForeColor="Red" ></asp:RequiredFieldValidator>
            <asp:TextBox ID="passw"  runat="server" TextMode="Password" BorderColor="gray" BorderWidth="2px"></asp:TextBox><br /><br />
            <asp:Label ID="Label2" runat="server" ForeColor="black" Text="Jepni pershkrimin"></asp:Label><br /><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            Text="*" ErrorMessage="Fusni Passwordin" ControlToValidate="TextArea1"
            ForeColor="Red" ></asp:RequiredFieldValidator>
            <asp:TextBox id="TextArea1" BorderColor="gray" BorderWidth="2px" TextMode="multiline" Columns="50" Rows="10" runat="server" /><br /><br />
            <asp:Button ID="Button1" runat="server" Text="Dergo" OnClick="Button1_Click" />
            <asp:Label ID="Lbl" runat="server" ForeColor="black" Text=""></asp:Label><br />    <br />
 </div>

  <div style=" position:relative; float:left; width:400px; top:-400px; left:600px">
      <asp:Label ID="Label9" runat="server" CssClass="emri" ForeColor="green" Font-Size="25px" Text="Jepni Vleresimin Tuaj"></asp:Label> <br /><br />   <br />     
<table width="0%" border="0" class="basic11pt" style="left:600px">
 <tr>
  <td>Keq&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td><asp:radiobuttonlist RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rblRating" runat="server">
   <asp:listitem Value="1">1</asp:listitem>
   <asp:listitem Value="2">2</asp:listitem>
   <asp:listitem Value="3" selected="True">3</asp:listitem>
   <asp:listitem Value="4">4</asp:listitem>
   <asp:listitem Value="5">5</asp:listitem>
   </asp:radiobuttonlist></td>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mire</td>     
 </tr>
</table></br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <asp:button ID="btnRating" OnClick="btnRating_Click" runat="server" Font-Size="25px" CausesValidation="False" Text="Vlereso" />   <br /><br /><br /><br /><br />
      <asp:ImageButton ID="ImageButton1" runat="server"  CssClass="res" OnClick="reservo" CausesValidation="False" ImageUrl='images/reservo.jpg'/> </br></br>
                         
</div> 

 </div>
</asp:Content>
