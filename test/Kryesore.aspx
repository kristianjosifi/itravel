﻿<%@ Page Title="Kryesore" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Kryesore.aspx.cs" Inherits="ITravel.Kryesore" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVy-Bpck-CYS_bAf4iav7TRA6UWv-QT_E&callback=myMap"></script>

<script>
    var myCenter = new google.maps.LatLng(41.328071, 19.818628);

    function initialize() {
        var mapProp = {
            center: myCenter,
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map1"), mapProp);
        
        var marker = new google.maps.Marker({
            position: myCenter,
        });
        marker.setMap(map);
        var infowindow = new google.maps.InfoWindow({
            content: "Ju Jeni Ketu!"
        });
        infowindow.open(map, marker);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<style>
#menu li a:hover span span, #menu #menu_active1 a span span {
	background:url(../images/menu_bg.gif) top repeat-x
}
.map{
    width: 420px;
    height: 155px;
    top: 10px;
    float: left;
}
.map2{
    width: 620px;
    height: 275px;
    top: -10px;
    float: left;
}
.for_banners {
    position:relative;
    width:100%;
    height: 290px;
    background:#0094B0;
}
        .auto-style1 {
             position:relative;
             text-align:center;
             padding:10px;
             width:100%;
            
        }
        .auto-style2 {
            position:relative;
            text-align:center;
            left:8px;
            top:7px;
            background-color:#0094B0;
            border:3px solid white;
            border-radius:10px 10px;
            /*padding:10px;*/
             width:100%;
             float: left;
}
        .tedhena{
            float:left;
            width: 100%;
            height:170px;
        }
        .fotot{
             width:350px;
             height:150px;
             padding:10px;
             float: left;
        }
        #divv{
            position:relative;
              width:400px;
             height:150px;
             float: left;
        }
        .emri{
            color:black;
            font-size:15px;
            /*padding:10px 0 10px 0;*/
            text-align:center;
            font-weight:bold;
            float: left;
        }
        .shiko{
            height:70px;
            width:150px;
            position:relative;
            top:-100px;
            left:150px;
            border:5px solid green;
        }

        .log{color:firebrick;}
        .men{width:100px;}
        body{
             background-repeat:repeat-x;
             
         }
</style>
<section id="content">
    <div class="for_banners">
      <article class="col1">
        <div class="tabs" style="left:-15px;  top:-10px">
          <ul class="nav">
            <li><a class="selected" style="width:286px">Kerko</a></li>
          </ul>
          <div class="content">
            <div class="tab-content" id="Hotel">
              <form id="form_2" action="#" method="post">
                <div>
                    </br>
                  <div class="row" > <span class="left">Tipi</span>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <asp:DropDownList id="rol" runat="server" BorderColor="#7CB9E8" BorderWidth="1px">
                      <asp:ListItem Selected="True" Value="">  </asp:ListItem>
                  <asp:ListItem  Value="User"> User </asp:ListItem>
                  <asp:ListItem Value="Kompani"> Kompani </asp:ListItem>
               </asp:DropDownList> 
                  </div>
                  <div class="row"> <span class="left">Nisja </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="nisja" runat="server" BorderColor="#7CB9E8" BorderWidth="1px" TextMode="Date" ></asp:TextBox>
                  </div>
                  <div class="row"> <span class="left">Mberritja </span>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="mberritja" runat="server" BorderColor="#7CB9E8" BorderWidth="1px" TextMode="Date"></asp:TextBox>
                   </div>
                  <div class="row"> <span class="left">Nr Pasagjeresh</span>
                &nbsp; <asp:TextBox ID="nrpass" runat="server" BorderColor="#7CB9E8" BorderWidth="1px" ></asp:TextBox>
                       </div>
                  <div class="row"> <span class="left">Vendnisja</span>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="vendnisja" runat="server" BorderColor="#7CB9E8" BorderWidth="1px" ></asp:TextBox>  </div>
                  <div class="row"> <span class="left">Destinacioni</span>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <asp:TextBox ID="destinacioni" runat="server" BorderColor="#7CB9E8" BorderWidth="1px" ></asp:TextBox>
                     </div>    </br>
                  <div class="wrapper" style="left:-80px; position:relative"> <span class="right relative"><a href="#" class="button1"><strong><asp:Button ID="kerko" runat="server"  Text="Kerko" BackColor="Transparent" onclick="search_TextChanged"/></strong></a></span>  </div>
                </div>
              </form>
            </div>
         
             
          </div>
            
        </div>
          
      </article>
         <div class="map2" id="map1"></div>
     </div>
    <div class="wrapper pad1" style="background-color:#0094B0; top:-40px; position:relative">
        <asp:DataList ID="DataList1" runat="server" GridLines="Horizontal" style="background-color:#0094B0" RepeatColumns="1" Width="100%"  RepeatDirection="Horizontal" BackColor="#1c478e"  CellPadding="4"  >
              <ItemTemplate>
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style2">
                            <div class="tedhena" >
                                 <div id="divv"><asp:Image ID="foto" runat="server" CssClass="fotot" ImageUrl='<%# Eval("FOTO") %>'/></div><br />
                       
                            <asp:Label ID="Label8" runat="server" style="display:none" Text='<%# Eval("id") %>'></asp:Label>   
                            <asp:Label ID="Label1" runat="server" CssClass="emri" Text="Drejtuesi: &nbsp;&nbsp;&nbsp;"></asp:Label><asp:Label ID="drejtuesi" runat="server" CssClass="emri" Text='<%# Eval("drejtuesi") %>'></asp:Label>     <br />
                            <asp:Label ID="Label2" runat="server" CssClass="emri" Text="Numer Pasagjeresh: &nbsp;&nbsp;&nbsp;"></asp:Label> <asp:Label ID="nrpass" runat="server" CssClass="emri" Text='<%# Eval("nrpass") %>'></asp:Label><br />
                            <asp:Label ID="Label3" runat="server" CssClass="emri" Text="Data Nisjes: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="nisja" runat="server" CssClass="emri" Text='<%# Eval("nisja") %>'></asp:Label><br />
                            <asp:Label ID="Label5" runat="server" CssClass="emri" Text="Data Mberritjes: &nbsp;&nbsp;&nbsp;"></asp:Label> <asp:Label ID="mberritja" runat="server" CssClass="emri" Text='<%# Eval("mberritja") %>'></asp:Label><br />
                            <asp:Label ID="Label6" runat="server" CssClass="emri" Text="Vendi Nisjes: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="vendnisja" runat="server" CssClass="emri" Text='<%# Eval("vendnisja") %>'></asp:Label><br />
                            <asp:Label ID="Label7" runat="server" CssClass="emri" Text="Destinacioni: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="destinacioni" runat="server" CssClass="emri" Text='<%# Eval("destinacioni") %>'></asp:Label>   <br />
                                      
                            <asp:ImageButton ID="ImageButton1" runat="server"  CssClass="shiko" OnClick="kontaktouser" ImageUrl='images/shiko.jpg' PostBackUrl='<%#"Udhetim.aspx?id="+Eval("id") %>'/>
                                </div>
                            
                            </td>
                   
                      </tr>

                    <tr>
                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle  />
        </asp:DataList>
    </div>
  </section>

</asp:Content>
