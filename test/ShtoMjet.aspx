﻿<%@ Page Title="Shto mjet" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShtoMjet.aspx.cs" Inherits="ITravel.ShtoMjet" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<style>
#menu li a:hover span span, #menu #menu_active4 a span span {
	background:url(../images/menu_bg.gif) top repeat-x
}
</style>
 <div style="padding-left:30px; padding-top:10px; background-color:#0094B0;" >
       <div style="background-color:#0094B0 ; z-index:9; position:relative; top:10px; "> <asp:Label ID="Label9" runat="server" ForeColor="white" Font-Size="50px" Text="Shto Mjet" ></asp:Label>  </br> </div> </br>     </br> 
       <table cellspacing=4 cellpadding=2 width=100% >
        <tr>
            <td class="style1"><asp:Label ID="Label1" ForeColor="black" runat="server" Text="Jepni Marken" ></asp:Label>  </td>
            <td>
                <asp:TextBox ID="marka" runat="server" BorderColor="gray" BorderWidth="1px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="marka" 
                    ErrorMessage="Jepni Marken" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1"><asp:Label ID="Label2" ForeColor="black" runat="server" Text="Jepni Tipin" ></asp:Label></td>
            <td>
                <asp:TextBox ID="tipi" runat="server" BorderColor="gray" BorderWidth="1px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="tipi" 
                    ErrorMessage="Jepni Tipin" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1"><asp:Label ID="Label3" ForeColor="black" runat="server" Text="Jepni Vitin" ></asp:Label></td>
            <td>
                <asp:TextBox ID="viti" runat="server" MaxLength="4" TextMode="Number" BorderColor="gray" BorderWidth="1px"></asp:TextBox>
                 <asp:RangeValidator ID="RangeValidator1" ForeColor="Red" runat="server" MinimumValue="1900" ErrorMessage="Must enter a numeric value" ControlToValidate="viti" MaximumValue="2018" Type="Integer"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="viti" 
                    ErrorMessage="Jepni Vitin" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:
            </td>
        </tr>     
        <tr>
            <td class="style1"><asp:Label ID="Label4" ForeColor="black" runat="server" Text="Jepni Numrin e Pasagjereve" ></asp:Label></td>
            <td>
               <asp:DropDownList id="nrpass" runat="server" BorderColor="gray" BorderWidth="1px">

                  <asp:ListItem Selected="True" Value="1"> 1 </asp:ListItem>
                  <asp:ListItem Value="2"> 2 </asp:ListItem>
                  <asp:ListItem Value="3"> 3 </asp:ListItem>
                  <asp:ListItem Value="4"> 4 </asp:ListItem>
                  <asp:ListItem Value="5">5 </asp:ListItem>
                  <asp:ListItem Value="6">6 </asp:ListItem>
                  <asp:ListItem Value="7">7 </asp:ListItem>
                  <asp:ListItem Value="8">8 </asp:ListItem>
                   <asp:ListItem Value="9">9 </asp:ListItem>
                   <asp:ListItem Value="10">10 </asp:ListItem>
                   <asp:ListItem Value="11">11 </asp:ListItem>
                   <asp:ListItem Value="12">12 </asp:ListItem>
               </asp:DropDownList>    </td>
        </tr>
        <tr>
            <td class="style1"><asp:Label ID="Label5" ForeColor="black" runat="server" Text="Jepni Foton" ></asp:Label></td>
            <td>
                <asp:FileUpload ID="IMAZHI"  runat="server" ForeColor="black"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="red" runat="server" 
                    ControlToValidate="IMAZHI" ErrorMessage="Jepni nje imazh"></asp:RequiredFieldValidator>
            </td>
        </tr>
         <tr>
            <td class="style1"><asp:Label ID="Label7" ForeColor="black" runat="server" Text="Jepni Numrin e Shasise" ></asp:Label></td>
            <td>
                <asp:TextBox ID="shasi" runat="server" BorderColor="gray" BorderWidth="1px"></asp:TextBox >
             <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="Red" runat="server" ErrorMessage="Jepni nje Numer Shasie" ControlToValidate="shasi"></asp:RequiredFieldValidator>
            </td>
        </tr>
</table> <br />

    <asp:Button ID="Ruaj" runat="server"  Text="Ruaj" onclick="ruaj_Click"/>
    <asp:Label ID="Label6" runat="server" Text="" ForeColor="black"></asp:Label><br /><br />
 </div>
</asp:Content>
