﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UdhetimiIm.aspx.cs" Inherits="ITravel.UdhetimiIm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
       <style>

.map1{
    width: 420px;
    height: 475px;
    top: 10px;
    float: left;
}
.auto-style1 {
             position:relative;
           
             padding:10px;
             width:100%;
        }
.auto-style2 {
            position:relative;
           
            left:8px;
            top:7px;

             width:100%;
             float: left;
}
        .tedhena{
            float:left;
            width: 100%;
        }
        .fotot{
             width:450px;
             height:300px;
             padding:10px;
             float: left;
        }

        #mbaj{
            position:relative;
                   float:left;
        }
        .emri{
            color:black;
            font-size:15px;
            /*padding:10px 0 10px 0;*/
            text-align:center;
            font-weight:bold;
            float: left;
        }
        .bli{
            height:30px;
            width:30px;
        }
        .res {
            height:90px;
            width:200px;
            border:5px solid blue;
            position:relative;
        }
        .cmimi{
            bottom:20px;
            color:white;
            font-size:20px;
            padding:0px;
            text-align:center;
            font-weight:bold;
        }
        .log{color:firebrick;}
        .men{width:100px;}
        body{background-repeat:repeat-x;}
        #trup{ background-color:#0094B0;}
</style>
       <div id="trup">
        <asp:DataList ID="DataList1" runat="server" GridLines="Horizontal" RepeatColumns="1" Width="100%" RepeatDirection="Horizontal"  CellPadding="4"  >
              <ItemTemplate>
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style2">
                                </br>
                            <asp:Label ID="Label8" runat="server" ForeColor="black" Text="Detajet e Udhetimit" Font-Size="20px"></asp:Label></br>   </br>
                            <div id="divv"><asp:Image ID="foto" runat="server" CssClass="fotot" ImageUrl='<%# Eval("FOTO") %>'/></div><br />
                            <div id="mbaj">
                            <asp:Label ID="Label10" runat="server" CssClass="emri" Font-Size="30px" ForeColor="red" Text="Rate: &nbsp;&nbsp;&nbsp;"></asp:Label><asp:Label ID="Label11" ForeColor="red" Font-Size="30px" runat="server" CssClass="emri" Text='<%# Eval("rate") %>'></asp:Label><asp:Label ID="Label12" runat="server" CssClass="emri" Font-Size="30px" ForeColor="red" Text="&nbsp;/&nbsp;5"></asp:Label>     <br /> <br /><br />
                            <asp:Label ID="Label1" runat="server" CssClass="emri" Text="Drejtuesi: &nbsp;&nbsp;&nbsp;"></asp:Label><asp:Label ID="drejtuesi" runat="server" CssClass="emri" Text='<%# Eval("drejtuesi") %>'></asp:Label>     <br />
                            <asp:Label ID="Label2" runat="server" CssClass="emri" Text="Numer Pasagjeresh: &nbsp;&nbsp;&nbsp;"></asp:Label> <asp:Label ID="nrpass" runat="server" CssClass="emri" Text='<%# Eval("nrpass") %>'></asp:Label><br />
                            <asp:Label ID="Label3" runat="server" CssClass="emri" Text="Data Nisjes: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="nisja" runat="server" CssClass="emri" Text='<%# Eval("nisja") %>'></asp:Label><br />
                            <asp:Label ID="Label5" runat="server" CssClass="emri" Text="Data Mberritjes: &nbsp;&nbsp;&nbsp;"></asp:Label> <asp:Label ID="mberritja" runat="server" CssClass="emri" Text='<%# Eval("mberritja") %>'></asp:Label><br />
                            <asp:Label ID="Label6" runat="server" CssClass="emri" Text="Vendi Nisjes: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="vendnisja" runat="server" CssClass="emri" Text='<%# Eval("vendnisja") %>'></asp:Label><br />
                            <asp:Label ID="Label7" runat="server" CssClass="emri" Text="Destinacioni: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="destinacioni" runat="server" CssClass="emri" Text='<%# Eval("destinacioni") %>'></asp:Label>   <br />       <br />
                             </div>  </br>
                  <%--             <div class="map1" id="map"></div>--%>
                            </td>
                      </tr>
                    <tr>
                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle  />
        </asp:DataList>

        <asp:DataList ID="DataList2" runat="server" GridLines="Horizontal" RepeatColumns="1" Width="100%" RepeatDirection="Horizontal"  CellPadding="4"  >
              <ItemTemplate>
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style2">
                        
                            <asp:Label ID="Label8" runat="server" ForeColor="black" Text="Detajet e Automjetit" Font-Size="20px"></asp:Label></br>   </br>
                            <div id="divv"><asp:Image ID="foto" runat="server" CssClass="fotot" ImageUrl='<%# Eval("photo") %>'/></div><br />
                            <div id="mbaj">
                            <asp:Label ID="Label1" runat="server" CssClass="emri" Text="Marka: &nbsp;&nbsp;&nbsp;"></asp:Label><asp:Label ID="drejtuesi" runat="server" CssClass="emri" Text='<%# Eval("marka") %>'></asp:Label>     <br />
                            <asp:Label ID="Label2" runat="server" CssClass="emri" Text="Tipi: &nbsp;&nbsp;&nbsp;"></asp:Label> <asp:Label ID="nrpass" runat="server" CssClass="emri" Text='<%# Eval("tipi") %>'></asp:Label><br />
                            <asp:Label ID="Label3" runat="server" CssClass="emri" Text="Viti: &nbsp;&nbsp;&nbsp;"></asp:Label>  <asp:Label ID="nisja" runat="server" CssClass="emri" Text='<%# Eval("viti") %>'></asp:Label><br />
                            <asp:Label ID="Label5" runat="server" CssClass="emri" Text="Numri Vendeve: &nbsp;&nbsp;&nbsp;"></asp:Label> <asp:Label ID="mberritja" runat="server" CssClass="emri" Text='<%# Eval("nrvendeve") %>'></asp:Label><br />
                            </div>  </br>
                 
                            </td>
                      </tr>
                    <tr>
                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle  />
        </asp:DataList>
               
     <table cellspacing=4 cellpadding=2 width=100%  >
       <tr>
            <td class="style1"><asp:Label ID="Label4" ForeColor="black" runat="server" Text="Jepni Numrin e Pasagjereve" ></asp:Label></td>
            <td>
               <asp:DropDownList id="nrpass" runat="server" BorderColor="gray" BorderWidth="1px">
                  <asp:ListItem Selected="True" Value="1"> 1 </asp:ListItem>
                  <asp:ListItem Value="2"> 2 </asp:ListItem>
                  <asp:ListItem Value="3"> 3 </asp:ListItem>
                  <asp:ListItem Value="4"> 4 </asp:ListItem>
                  <asp:ListItem Value="5">5 </asp:ListItem>
                  <asp:ListItem Value="6">6 </asp:ListItem>
                  <asp:ListItem Value="7">7 </asp:ListItem>
                   <asp:ListItem Value="8">8 </asp:ListItem>
                   <asp:ListItem Value="9">9 </asp:ListItem>
                   <asp:ListItem Value="10">10 </asp:ListItem>
                   <asp:ListItem Value="11">11 </asp:ListItem>
                   <asp:ListItem Value="12">12 </asp:ListItem>
               </asp:DropDownList>    </td>
        </tr>
        <tr>
            <td class="style1"><asp:Label ID="Label1" ForeColor="black" runat="server" Text="Jepni Nisjen" ></asp:Label>  </td>
            <td>
                <asp:TextBox ID="nisja" runat="server" TextMode="Date" BorderColor="gray" BorderWidth="1px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="nisja" 
                    ErrorMessage="Jepni Nisjen" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1"><asp:Label ID="Label2" ForeColor="black" runat="server" Text="Jepni Mberritjen" ></asp:Label></td>
            <td>
                <asp:TextBox ID="mberritja" runat="server" TextMode="Date" BorderColor="gray" BorderWidth="1px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="mberritja" 
                    ErrorMessage="Jepni Mberritjen" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1"><asp:Label ID="Label8" ForeColor="black" runat="server" Text="Jepni Vendnisjen" ></asp:Label></td>
            <td>
                <asp:TextBox ID="vendnisja" runat="server" BorderColor="gray" BorderWidth="1px" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="vendnisja" 
                    ErrorMessage="Jepni Vendnisjen" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1"><asp:Label ID="Label9" ForeColor="black" runat="server" Text="Jepni Destinacionin" ></asp:Label></td>
            <td>
                <asp:TextBox ID="dest" runat="server" BorderColor="gray" BorderWidth="1px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="dest" 
                    ErrorMessage="Jepni Destinacionin" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
       
        <tr>
            <td class="style1"><asp:Label ID="Label5" ForeColor="black" runat="server" Text="Jepni Foton" ></asp:Label></td>
            <td>
                <asp:FileUpload ID="IMAZHI"  runat="server" ForeColor="black" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="red" runat="server" 
                    ControlToValidate="IMAZHI" ErrorMessage="Jepni nje imazh"></asp:RequiredFieldValidator>
            </td>
        </tr>
</table></br>

  <div style=" position:relative; float:left; width:600px; top:-200px; left:700px">
    
      <asp:button ID="btnRating" OnClick="Fshi" runat="server" Width="150px" Height="50px" ForeColor="red" Font-Size="25px" Text="Fshi" />   <br /><br /><br />
        <asp:button ID="Button1" OnClick="Update" runat="server" Width="150px" Height="50px" ForeColor="blue" Font-Size="25px" Text="Update" />   <br /><br /><br />
                       
</div> 

 </div>
</asp:Content>
