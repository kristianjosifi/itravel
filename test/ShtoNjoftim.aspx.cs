﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Configuration;

namespace ITravel
{
    public partial class ShtoNjoftim : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ruaj_Click(object sender, EventArgs e)
        {
            string useri = Session["Username"].ToString();
            string roli = Session["Roli"].ToString();
            string s = @"~\foto\" + IMAZHI.FileName;
            IMAZHI.PostedFile.SaveAs(Server.MapPath(s));
            string connectionString = WebConfigurationManager.ConnectionStrings["ITravel"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string shto = "INSERT INTO udhetim(drejtuesi,nrpass ,nisja ,mberritja ,vendnisja, destinacioni, foto,rate, roli,username ) VALUES((select emri from person where username=@useri),@nrpass , @nisja, @mberritja, @vendnisja,@destinacioni,@photo,1,@roli,@useri)";
                SqlCommand cmd = new SqlCommand(shto, con);
                cmd.Parameters.AddWithValue("@nrpass", nrpass.Text);
                cmd.Parameters.AddWithValue("@vendnisja", vendnisja.Text);
                cmd.Parameters.AddWithValue("@nisja", nisja.Text);
                cmd.Parameters.AddWithValue("@roli", roli);
                cmd.Parameters.AddWithValue("@useri", useri);
                cmd.Parameters.AddWithValue("@mberritja", mberritja.Text);
                cmd.Parameters.AddWithValue("@destinacioni", dest.Text);
                cmd.Parameters.AddWithValue("@photo", s);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                Label6.Text = "Udhetimi u Shtua";
            }
        }
    }
}